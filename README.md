# mybatis-cipher

#### 项目介绍
基于mybatis 来实现对敏感数据在进出DB时候进行脱敏处理，
让各位码友们无需自己各种手动实现


#### 安装教程

1. 下载最新的jar包 `mybatis-cipher-plus`

#### 使用说明
##### 配置初始化
1. spring boot 模式 零配置模式

配置加解密拦截器
```java
/**
 * MybatisPlusConfig
 * @author Jerry.hu
 * @summary MybatisPlusConfig
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description MybatisPlusConfig
 * @since 2018-09-29 16:46
 */
@EnableTransactionManagement
@Configuration
public class MybatisPlusConfig {

    @Bean
    public FieldEncryptInterceptor paginationInterceptor() {
        return new FieldEncryptInterceptor();
    }
}

``` 
yml配置
```yml
mybatis:
    type-aliases-package: org.xiaoweige.mybatis.cipher.example.model.po
    mapper-locations:
      - classpath:mybatis/mappers/*.xml
      - classpath:mybatis/mappers/*/*.xml
    configuration-properties:
      cacheEnabled : false
       ####<!-- 查询时，关闭关联对象即时加载以提高性能 -->
      lazyLoadingEnabled : true
       ####<!-- 设置关联对象加载的形态，此处为按需加载字段(加载字段由SQL指定)，不会加载关联表的所有字段，以提高性能 -->
      aggressiveLazyLoading : false
       ####<!-- 对于未知的SQL查询，允许返回不同的结果集以达到通用的效果 -->
      multipleResultSetsEnabled : true
       ####<!-- 允许使用列标签代替列名 -->
      useColumnLabel : true
       ####<!-- 允许使用自定义的主键值(比如由程序生成的UUID 32位编码作为键值)，数据表的PK生成策略将被覆盖 -->
      useGeneratedKeys : true
       ####<!-- 给予被嵌套的resultMap以字段-属性的映射支持 -->
      autoMappingBehavior : FULL
       ####<!-- 对于批量更新操作缓存SQL以提高性能 -->
      defaultExecutorType : SIMPLE
       ####<!-- 数据库超过15秒仍未响应则超时，部分语句可单独指定超时时间 -->
      defaultStatementTimeout : 15
      jdbcTypeForNull : NULL
       ####<!-- 当参数为NULL且字段列可为空的Double等类型时可直接当NULL插入 -->
      callSettersOnNulls : true
      ####打印sql语句###
      logPrefix : "dao."
```
2. xml 配置方式
```java
<settings>
		<!-- 全局映射器启用缓存 -->
		<setting name="cacheEnabled" value="false" />
		<!-- 查询时，关闭关联对象即时加载以提高性能 -->
		<setting name="lazyLoadingEnabled" value="true" />
		<!-- 设置关联对象加载的形态，此处为按需加载字段(加载字段由SQL指定)，不会加载关联表的所有字段，以提高性能 -->
		<setting name="aggressiveLazyLoading" value="false" />
		<!-- 对于未知的SQL查询，允许返回不同的结果集以达到通用的效果 -->
		<setting name="multipleResultSetsEnabled" value="true" />
		<!-- 允许使用列标签代替列名 -->
		<setting name="useColumnLabel" value="true" />
		<!-- 允许使用自定义的主键值(比如由程序生成的UUID 32位编码作为键值)，数据表的PK生成策略将被覆盖 -->
		<setting name="useGeneratedKeys" value="true" />
		<!-- 给予被嵌套的resultMap以字段-属性的映射支持 -->
		<setting name="autoMappingBehavior" value="FULL" />
		<!-- 对于批量更新操作缓存SQL以提高性能 -->
		<setting name="defaultExecutorType" value="SIMPLE" />
		<!-- 数据库超过15秒仍未响应则超时，部分语句可单独指定超时时间 -->
		<setting name="defaultStatementTimeout" value="15" />
		<setting name="jdbcTypeForNull" value="NULL" />
		<!-- 当参数为NULL且字段列可为空的Double等类型时可直接当NULL插入 -->
		<setting name="callSettersOnNulls" value="true" />
		<!--&lt;!&ndash; 打印sql语句 &ndash;&gt;-->
		<setting name="logPrefix" value="dao." />
	</settings>

	<plugins>
		<plugin interceptor="org.xiaoweige.mybatis.interceptor.FieldEncryptInterceptor">
		</plugin>
	</plugins>
```
##### 实现加解密接口 CryptService
```java
/**
 * 加解密服务接口
 * @author Jerry.hu
 * @summary 加解密服务接口
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description 加解密服务接口
 * @since 2018-09-29 10:58:18
 */
public interface CryptService {
    /**
     * 单条加密
     * @author Jerry.hu
     * @modifier Jerry.hu
     * @since 2018-09-29 10:58:18
     * @param value 待加密字段
     * @return 加密后的字符串
     */
    String encrypt(String value);

    /**
     * 单条解密
     * @author Jerry.hu
     * @modifier Jerry.hu
     * @since 2018-09-29 10:58:18
     * @param value 待解密字段
     * @return  解密后的字符串
     */
    String decrypt(String value);

    /**
     * 批量解密
     * @author Jerry.hu
     * @modifier Jerry.hu
     * @since 2018-09-29 10:58:18
     * @param ori 待解密密文集合
     * @return map key 密文 value 明文
     */
    Map<String,String> batchDecrypt(List<String> ori);

    /**
     * 批量加密
     * @author Jerry.hu
     * @modifier Jerry.hu
     * @since 2018-09-29 10:59:36
     * @param ori 待解密密文集合
     * @return map key 明文  value 原密
     */
    Map<String,String> batchEncrypt(List<String> ori);

}
```
建议将接口中的四个接口全部实现
