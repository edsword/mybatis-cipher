package org.xiaoweige.mybatis.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * encrypted
 * @author Jerry.hu
 * @summary encrypted
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description encrypted
 * @since 2018-06-10 15:11
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.FIELD})
public @interface Encrypted {

}
