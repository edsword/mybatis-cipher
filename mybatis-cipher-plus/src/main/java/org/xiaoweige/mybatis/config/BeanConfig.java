package org.xiaoweige.mybatis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * BeanConfig
 * @author Jerry.hu
 * @summary BeanConfig
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description BeanConfig
 * @since 2018-06-11 18:57
 */
@Configuration
public class BeanConfig {

    @Bean
    public BeanFactoryHolder init() {
        return new BeanFactoryHolder();
    }

}
