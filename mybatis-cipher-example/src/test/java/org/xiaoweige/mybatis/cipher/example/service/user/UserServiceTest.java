package org.xiaoweige.mybatis.cipher.example.service.user;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.xiaoweige.mybatis.cipher.example.model.po.User;
import org.xiaoweige.mybatis.cipher.example.model.po.User2;
import org.xiaoweige.mybatis.typeexample.BaseTest;

/**
 * UserServiceTest
 * @author Jerry.hu
 * @summary UserServiceTest
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description UserServiceTest
 * @since 2018-06-07 14:19
 */
public class UserServiceTest extends BaseTest {

    @Autowired
    private UserService userService;



    @Test
    public void testSingleInsertAndReturnId() {
        User user = new User();
        user.setName("张三");
        user.setMobile("18621212112121");
        this.userService.singleInsertAndReturnId(user);
        System.out.println(user);
    }

    @Test
    public void testSingleInsert() {
        User user = new User();
        user.setName("张三");
        user.setMobile("18634343342342");
        user.setId(1L);
        this.userService.singleInsert(user);
        System.out.println(user);
    }

    @Test
    public void testListInsert() {
        int page = 1;
        while (page<2){
        ArrayList<User> arrayList = new ArrayList<>();
        for(int i =0;i<2;i++){
            User user = new User();
            user.setName("张三");
//            if(i==0){
//                user.setMobile("0");
//            }else if(i < 10){
//                user.setMobile("0.00");
//            }
//            else  if(i <40){
//                user.setMobile("0.000");
//            }
//            else {
                user.setMobile(String.valueOf(i));
//            }
            user.setName("aaaa"+i);
            user.setAccount(i+"");
            arrayList.add(user);
        }
//        arrayList.parallelStream().forEach(user -> {
//            user.setAccount(String.valueOf(Math.random()));
//            try {
//                this.userService.singleInsert(user);
//            }catch (Exception e){
//
//            }
//        });
            this.userService.insertList(arrayList);
        arrayList.forEach(System.out::println);
//        this.userService.insertList(arrayList);
        page++;
        }
    }


    @Test
    @Transactional
    public void testFindById() {
        User user = this.userService.findById(13);
        System.out.println(user);
    }

    @Test
    @Transactional
    public void testFindList() {
        List<User> userList2 = this.userService.findList();
        userList2.forEach(System.out::println);
    }

    @Test
    public void testFindOne() {
        User2 user2 = new User2();
        user2.setAccount("aaa");
        System.out.println(user2.hashCode());
        System.out.println("====================");
        user2.setAccount("aaccccc");
        System.out.println(user2.hashCode());
    }
}
