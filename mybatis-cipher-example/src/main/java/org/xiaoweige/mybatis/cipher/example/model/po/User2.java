package org.xiaoweige.mybatis.cipher.example.model.po;

import org.xiaoweige.mybatis.annotation.Encrypted;

/**
 * U色弱
 * @author Jerry.hu
 * @summary U色弱
 * @Copyright (c) 2018, Ke Group All Rights Reserved.
 * @Description U色弱
 * @since 2018-11-05 17:07
 */
public class User2 extends User{

    private static final long serialVersionUID = 7790712134566751498L;
    /**
     * 账户余额
     */
    @Encrypted
    private String account;


    /**
     * 获取 账户余额
     */
    @Override
    public String getAccount() {
        return this.account;
    }

    /**
     * 设置 账户余额
     */
    @Override
    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "User2{" +
                "account='" + account + '\'' +
                '}';
    }
}
