package org.xiaoweige.mybatis.cipher.example.service.user;

import org.springframework.stereotype.Service;
import org.xiaoweige.mybatis.cipher.example.model.po.User;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * UserService
 * @author Jerry.hu
 * @summary UserService
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description UserService
 * @since 2018-06-07 14:07
 */
@Service
public class UserService {

    @Resource
    private UserMapper userMapper;

    public void singleInsertAndReturnId(User user){
        this.userMapper.singleInsertAndReturnId(user);
    }

    public void singleInsert(User user){
        this.userMapper.singleInsert(user);
    }


    public void insertList(ArrayList<User> asList) {
        this.userMapper.insertList(asList);
    }

    public User findById(long id){
        return this.userMapper.findById(id);
    }

    public List<User> findList() {
        return this.userMapper.findList();
    }
}
