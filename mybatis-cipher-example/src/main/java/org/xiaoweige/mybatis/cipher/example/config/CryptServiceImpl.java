package org.xiaoweige.mybatis.cipher.example.config;

import org.springframework.stereotype.Service;
import org.xiaoweige.mybatis.annotation.CryptService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CryptServiceImpl
 * @author Jerry.hu
 * @summary CryptServiceImpl
 * @Copyright (c) 2018, xiaoweige Group All Rights Reserved.
 * @Description CryptServiceImpl
 * @since 2018-06-10 15:55
 */
@Service
public class CryptServiceImpl implements CryptService {

    @Override
    public String encrypt(String value) {
        return TestEncryptCoderDemoUtil.encryptData(value);
    }

    @Override
    public String decrypt(String value) {
        return TestEncryptCoderDemoUtil.decryptData(value);
    }

    @Override
    public Map<String, String> batchDecrypt(List<String> ori) {
        Map<String,String> decrypts = new HashMap<>(ori.size());
        for(String orj : ori){
            decrypts.put(orj, TestEncryptCoderDemoUtil.decryptData(orj));
        }
        return decrypts;
    }

    @Override
    public Map<String, String> batchEncrypt(List<String> ori) {
        Map<String,String> decrypts = new HashMap<>(ori.size());
        for(String orj : ori){
            decrypts.put(orj, TestEncryptCoderDemoUtil.encryptData(orj));
        }
        return decrypts;
    }
}
